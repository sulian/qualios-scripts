# Script utilisateur Qualios

Ce script permet d'ajouter des fonctions personnalisées à Qualios.

Le suivi des modifications se fait depuis le fichier CHANGELOG.

## Installation

1. Copier le fichier `fonction_client.js` sur le serveur de développement ou de production
1. Depuis le terminal, copier le fichier dans `/var/www/html/SITE_QUALIOS/src/` (ex. `sudo cp fonction_client.js /var/www/html/SITE_QUALIOS/src/fonction_client.js`)

## Utilisation

Dans l'éditeur JavaScript de Qualios, les fonctions sont disponibles dans le menu `FONCTIONS PERSONNALISEES` 

## Documentation

Chaque fonction est documenté sur le wiki du serveur GitLab interne dans le projet [Qualios](http://gitlab.exsymol.mc/others/qualios/wikis/home)
