"use strict";
//---name : Calcul R&eacute;f&eacute;rence
//---Label : Fonction qui calcule automatiquement la r&eacute;f&eacute;rence des enregistrements. Le calcul est enregistr&eacute; dans `referenceFinale` et `anneeCreation`
//---nbParam : 5
//---param1 baseReference : Composant R&eacute;f&eacute;rence de base=>7
//---param1 fullDate : Date de cr&eacute;ation=>F
//---param1 idFiche : Composant Id. de la fiche=>1
//---param1 referenceFinale : Resultat R&eacute;f&eacute;rence calcul&eacute;e=>7
//---param1 anneeCreation : Resultat Ann&eacute;e de cr&eacute;ation=>7
//---type : 11
function calculRef(baseReference, fullDate, idFiche, referenceFinale, anneeCreation) {
    //---start
    var annee = document.getElementById(anneeCreation).value;
    if (annee.length == 0) {
        var monAnnee = document.getElementById(fullDate).value.substring(6, 10);
        document.getElementById(anneeCreation).value = monAnnee;
        var reference = document.getElementById(referenceFinale).value;
        var base = document.getElementById(baseReference).value;
        var id = document.getElementById(idFiche).value;
        if (reference.length == 0) {
            document.getElementById(referenceFinale).value = base + monAnnee + '-' + id;
        }
    }
    return true;
    //---End
}
//---name : Compare arrays
//---Label : Comparaison de deux tableaux
//---nbParam : 2
//---param3 array1 : Premi&egrave;re variable de type `array`
//---param3 array2 : Seconde variable de type `array`
//---type : 11
function compareArray(array1, array2) {
    //---start
    return array1.length === array2.length && array1.sort().every(function (value, index) {
        return value === array2.sort()[index];
    });
    //---End
}
