# Changelog
Tous les changements notables sur ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/fr/1.0.0/)
et suis les recommandations du [Versionnage Sémantique](http://semver.org/lang/fr/spec/v2.0.0.html).

## [Non publié] - 2018-12-11

### Ajouté

Utilisation du mode strict

### Corrigé

Afin d'éviter les problèmes d'encodage (UTF8 / western-1252), utilisation de la représentation HTML pour les caractères accentués présent dans les fichiers sources.

### Changement

Le projet se base maintenant sur typescript. Dorénavant, on utilisera plusieurs fichiers source et une compilation vers un seul fichier javascript.

## [0.2.1] - 2018-11-28

### Corrigé

- Fonction `CalculRef` : ajout du paramètre "referenceFinale" pour plus de flexibilité

## [0.2.0] - 2018-11-27

### Ajouté

- Fonction `CalculRef`

## [0.1.1] - 2018-11-22

### Corrigé

- Fonction `CompareArray` : correction des majuscules / minuscules pour prise en compte de tous les paramètres dans Qualios Manager

## [0.1.0] - 2018-11-22

### Ajouté

- Fonction `CompareArray`
