//---name : Compare arrays
//---Label : Comparaison de deux tableaux
//---nbParam : 2
//---param3 array1 : Premi&egrave;re variable de type `array`
//---param3 array2 : Seconde variable de type `array`
//---type : 11
function compareArray(array1: string[], array2: string[]) {
//---start
    return array1.length === array2.length && array1.sort().every(function(value, index) {
        return value === array2.sort()[index]
    });
//---End
}
