//---name : Calcul R&eacute;f&eacute;rence
//---Label : Fonction qui calcule automatiquement la r&eacute;f&eacute;rence des enregistrements. Le calcul est enregistr&eacute; dans `referenceFinale` et `anneeCreation`
//---nbParam : 5
//---param1 baseReference : Composant R&eacute;f&eacute;rence de base=>7
//---param1 fullDate : Date de cr&eacute;ation=>F
//---param1 idFiche : Composant Id. de la fiche=>1
//---param1 referenceFinale : Resultat R&eacute;f&eacute;rence calcul&eacute;e=>7
//---param1 anneeCreation : Resultat Ann&eacute;e de cr&eacute;ation=>7
//---type : 11
function calculRef(baseReference, fullDate, idFiche, referenceFinale, anneeCreation) {
//---start
    let annee = (<HTMLInputElement>document.getElementById(anneeCreation)).value

    if (annee.length == 0) {
        let monAnnee = (<HTMLInputElement>document.getElementById(fullDate)).value.substring(6, 10);
        (<HTMLInputElement>document.getElementById(anneeCreation)).value = monAnnee

        let reference = (<HTMLInputElement>document.getElementById(referenceFinale)).value
        let base = (<HTMLInputElement>document.getElementById(baseReference)).value
        let id = (<HTMLInputElement>document.getElementById(idFiche)).value

        if (reference.length == 0) {
            (<HTMLInputElement>document.getElementById(referenceFinale)).value = base + monAnnee + '-' + id
        }
    }
    return true;
//---End
}